package guide.albissat.io.al_bissat_guide.ui.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import guide.albissat.io.al_bissat_guide.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mAuth = FirebaseAuth.getInstance()

        alreadyButton.setOnClickListener {

            val newIntent = Intent(this, RegisterActivity::class.java)
            if(intent.extras!=null )
            {
                Log.d("tagii","new intent "+intent.extras.toString())
                newIntent.putExtras(intent.extras)
            }
            startActivity(newIntent)
        }

        loginButton.setOnClickListener {

            val emailUser = emailEdit.text
            val passwordUser = passwordEdit.text

            if (emailUser.isEmpty() || passwordUser.isEmpty()) {
                Toast.makeText(applicationContext, getText(R.string.empty_error), Toast.LENGTH_SHORT).show()
            } else {
                signInUser(emailEdit.text.toString(), passwordEdit.text.toString())
            }

        }
    }

    /**
     * Sign in user with email and password
     */
    private fun signInUser(email: String, password: String) {

        loaderContent.visibility = View.VISIBLE

        mAuth!!.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    loaderContent.visibility = View.GONE
                    if (task.isSuccessful) {
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(applicationContext, getText(R.string.auth_error), Toast.LENGTH_SHORT).show()
                    }

                }

    }

}
