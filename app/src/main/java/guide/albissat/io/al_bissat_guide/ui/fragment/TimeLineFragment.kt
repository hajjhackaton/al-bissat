package guide.albissat.io.al_bissat_guide.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import guide.albissat.io.al_bissat_guide.R
import kotlinx.android.synthetic.main.fragment_timeline.*

class TimeLineFragment(var currentStep: Int) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_timeline, container, false)

    companion object {
        fun newInstance(currentStep: Int): TimeLineFragment = TimeLineFragment(currentStep)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        when ( currentStep)
        {
            -1 -> {
                cardViewFlight.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
                cardViewLuggage.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
                cardViewLoop.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
                cardViewDelivered.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
            }
            0 -> {
                cardViewFlight.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.blue_light))
                cardViewLuggage.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
                cardViewLoop.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
                cardViewDelivered.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
            }

            1 -> {
                cardViewFlight.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.blue_light))
                cardViewLuggage.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.yellow))
                cardViewLoop.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
                cardViewDelivered.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
            }

            2 -> {
                cardViewFlight.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.blue_light))
                cardViewLuggage.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.yellow))
                cardViewLoop.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.red_light))
                cardViewDelivered.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey_light))
            }
            3 -> {
                cardViewFlight.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.blue_light))
                cardViewLuggage.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.yellow))
                cardViewLoop.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.red_light))
                cardViewDelivered.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.green_light))
            }
        }
    }
}