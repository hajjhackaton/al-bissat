package guide.albissat.io.al_bissat_guide.ui.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import guide.albissat.io.al_bissat_guide.R
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        signInButton.setOnClickListener {
            finish()
        }

        registerButton.setOnClickListener {

            val emailUser = emailEdit.text
            val passwordUser = passwordEdit.text

            if (emailUser.isEmpty() || passwordUser.isEmpty()) {
                Toast.makeText(applicationContext, getText(R.string.empty_error), Toast.LENGTH_SHORT).show()
            } else {
                signUpUser(emailEdit.text.toString(), passwordEdit.text.toString())
            }
        }
    }

    /**
     * Sign up user with email and password
     */
    private fun signUpUser(email: String, password: String) {

        loaderContent.visibility = View.VISIBLE

        mAuth!!.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    loaderContent.visibility = View.GONE
                    if (task.isSuccessful) {
                        var newIntent = Intent(this, MainActivity::class.java)
                        if(intent.extras!=null )
                        {
                            Log.d("tagii","new intent "+intent.extras.toString())
                            newIntent.putExtras(intent.extras)
                        }
                        startActivity(newIntent)
                        finish()
                    } else {
                        Toast.makeText(applicationContext, getText(R.string.register_error), Toast.LENGTH_SHORT).show()
                    }
                }

    }


}
