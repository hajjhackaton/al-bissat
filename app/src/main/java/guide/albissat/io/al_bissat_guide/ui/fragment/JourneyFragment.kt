package guide.albissat.io.al_bissat_guide.ui.fragment

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import guide.albissat.io.al_bissat_guide.*
import android.widget.AdapterView
import guide.albissat.io.al_bissat_guide.R
import guide.albissat.io.al_bissat_guide.adapter.CustomDropDownAdapter
import guide.albissat.io.al_bissat_guide.ui.activity.ListLuggageActivity
import guide.albissat.io.al_bissat_guide.ui.activity.MainActivity
import guide.albissat.io.al_bissat_guide.util.country
import kotlinx.android.synthetic.main.fragment_journey.*
import java.text.SimpleDateFormat
import java.util.*

class JourneyFragment : Fragment(), AdapterView.OnItemSelectedListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_journey, container, false)

    companion object {
        fun newInstance(): JourneyFragment = JourneyFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonAddLuggage.setOnClickListener {
            val intent = Intent(activity, ListLuggageActivity::class.java)
            startActivity(intent)
        }

        buttonSave.setOnClickListener {

            loaderContent.visibility = View.VISIBLE

            Handler().postDelayed({
                (activity as MainActivity).moveToTimeline()

            }, 1000)
        }

        var spinnerAdapter: CustomDropDownAdapter = CustomDropDownAdapter(context!!, country)
        spinnerCountry?.adapter = spinnerAdapter

        handleDateJourney()

    }

    override fun onResume() {
        super.onResume()

        if (flightIdEdit.text.isNotEmpty() && dateEdit.text.isNotEmpty()
                && listLuggage.isNotEmpty()) {
            buttonSave.visibility = View.VISIBLE
        } else buttonSave.visibility = View.GONE

    }

    private fun handleDateJourney() {

        var cal = Calendar.getInstance()
        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = "dd / MM / yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            dateEdit.setText(sdf.format(cal.time))

        }

        dateEdit.setOnClickListener {

            DatePickerDialog(context, dateSetListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
    }
}