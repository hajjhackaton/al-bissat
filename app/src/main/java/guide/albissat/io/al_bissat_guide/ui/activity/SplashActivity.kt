package guide.albissat.io.al_bissat_guide.ui.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)




        val newIntent = Intent(this, MainActivity::class.java)
        if(intent.extras!=null )
        {
            Log.d("tagii","new intent "+intent.extras.toString())
            newIntent.putExtras(intent.extras)
        }
        startActivity(newIntent)
        finish()
    }
}
