package guide.albissat.io.al_bissat_guide.ui.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import guide.albissat.io.al_bissat_guide.R
import guide.albissat.io.al_bissat_guide.ui.fragment.JourneyFragment
import guide.albissat.io.al_bissat_guide.ui.fragment.TimeLineFragment
import guide.albissat.io.al_bissat_guide.ui.fragment.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*
import android.util.Log
import guide.albissat.io.al_bissat_guide.listLuggage

class MainActivity : AppCompatActivity() {

    val TAG = MainActivity::class.java.name;

    private var currentStep: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        if (intent.extras != null && intent.extras.containsKey("step")) {
            Log.d("tagii", "new main intent step " + intent.extras.getString("step"))
            currentStep = intent.extras.getString("step").toInt()

            bottomNavigation.selectedItemId = R.id.navigation_timeline

        } else {
            bottomNavigation.selectedItemId = R.id.navigation_journeys
        }

    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        if (intent?.extras != null) {
            Log.d("tagii", "onNewIntent intent " + intent.extras.toString())
        }
    }

    /**
     * Handler navigation between bottom menu items
     */
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_journeys -> {

                val journeyFragment = JourneyFragment.newInstance()
                openFragment(journeyFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_timeline -> {

                val luggageFragment = TimeLineFragment.newInstance(currentStep)
                openFragment(luggageFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {

                val profileFragment = ProfileFragment.newInstance()
                openFragment(profileFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    /**
     * Open fragment when user click on icon bottom bar
     */
    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    public fun moveToTimeline() {
        if(listLuggage.isNotEmpty())
            currentStep = 1
        bottomNavigation.selectedItemId = R.id.navigation_timeline
    }

}
