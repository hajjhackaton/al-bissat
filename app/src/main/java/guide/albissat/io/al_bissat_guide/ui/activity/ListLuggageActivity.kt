package guide.albissat.io.al_bissat_guide.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.zxing.integration.android.IntentIntegrator
import guide.albissat.io.al_bissat_guide.R
import guide.albissat.io.al_bissat_guide.adapter.LuggageAdapter
import guide.albissat.io.al_bissat_guide.domain.Luggage
import guide.albissat.io.al_bissat_guide.listLuggage
import kotlinx.android.synthetic.main.activity_list_luggage.*

class ListLuggageActivity : AppCompatActivity() {

    val TAG = ListLuggageActivity::class.java.name;

    private lateinit var adapterLuggage: LuggageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_luggage)

        fabAddLuggage.setOnClickListener { openQRCodeIntent() }

        adapterLuggage = LuggageAdapter(this, listLuggage,
                { itemClicked, positionClicked ->
                }, { itemToDelete, positionToDelete ->

            listLuggage.removeAt(positionToDelete)
            adapterLuggage.notifyItemRemoved(positionToDelete)

        })

        recyclerViewLuggage.layoutManager = LinearLayoutManager(this)
        recyclerViewLuggage.adapter = adapterLuggage

        buttonSubmit.setOnClickListener {
            handlerSubmit()
        }

    }

    private fun handlerSubmit() {

        loaderContent.visibility = View.VISIBLE

        Handler().postDelayed({

            val returnIntent = Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish()

        }, 1000)
    }

    private fun openQRCodeIntent() {
        IntentIntegrator(this).initiateScan()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
            } else {
                listLuggage.add(Luggage(result.contents, result.contents))
                if (listLuggage.size > 0) emptyText.visibility = View.GONE
                adapterLuggage.notifyDataSetChanged()

                Log.d(TAG, "listLuggage size " + listLuggage.size)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }

    }

}
