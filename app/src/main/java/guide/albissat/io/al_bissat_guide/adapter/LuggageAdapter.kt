package guide.albissat.io.al_bissat_guide.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chauthai.swipereveallayout.SwipeRevealLayout
import guide.albissat.io.al_bissat_guide.R
import guide.albissat.io.al_bissat_guide.domain.Luggage
import kotlinx.android.synthetic.main.item_luggage.view.*

class LuggageAdapter(val activity: Activity, val listAdapter: List<Luggage>, val listener: (Luggage, Int) -> Unit,val listenerDelete: (Luggage, Int) -> Unit) :
        RecyclerView.Adapter<LuggageAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(activity, listAdapter[position], listener,listenerDelete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_luggage, parent, false))
    }

    override fun getItemCount(): Int {
        return listAdapter.size
    }

    /**
     * Binding data
     */
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(activity: Activity, luggage: Luggage, listener: (Luggage, Int) -> Unit, listenerDelete: (Luggage, Int) -> Unit) = with(itemView) {

            itemView.textviewLuggageId.text = luggage.id
            itemView.view_delete.setOnClickListener { listenerDelete(luggage,adapterPosition) }
            setOnClickListener { listener(luggage, adapterPosition) }
        }
    }
}